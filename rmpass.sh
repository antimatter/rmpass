#! /bin/bash
## vim: ai:si:et:sw=4:ts=4:sts=4:bs=2:sta:sr

ENV_me=$(basename $0);
ENV_medir="$(dirname $0)";
[ "$ENV_medir" = "." ] && ENV_medir="$(pwd)";
ENV_retval=0;
TIMESTAMP=$(date +%F_%H-%M-%S)

ENV_short_options='l:vVp:b:P:t';
ENV_long_options='log-file:,verbose,quiet,silent,help,version,password,backup-dir,password-file:,timestamp';

ENV_timestamp_in_log=0;     # 0 = Disabled, 1 = Enabled
DEF_log_file="/dev/null";
DEF_verbosity=2;
ENV_debug=1;
DEF_tmpdir="/tmp";              ## Temporary directory. Defined later
DEF_backup_dir="encrypted_pdfs";

ENV_version="0.0";
ENV_author="Gururaj K";
ENV_copyright_years=$(date "+%Y");

PASSWORDS=()

function print_help () {
    cat <<-_HELP_
Usage: $ENV_me [OPTION]... [FILE]...
<DESCRIPTION HERE>.

Mandatory arguments to long options are mandatory for short options too.
  -b, --backup-dir=DIR      use DIR to save the original files
  -p, --password=PASSWORD   use PASSWORD to decrypt the input files
  -P, --password-file=FILE  use passwords in FILE to decrypt the input files
  -t, --timestamp           append timestamps during file backup

      --help    display this help and exit
      --version output version information and exit
_HELP_
}

function timestamp () {
    echo "$(date +%F\ %T)";
}

function log () {
    if [ "$ENV_timestamp_in_log" = "1" ]; then
        echo -e "$ENV_me: $(timestamp): $@" | tee -a $log;
    else
        echo -e "$ENV_me: $(timestamp): $@";
    fi
}

function debug () {
    if [ "$ENV_debug" = 1 -o "$ENV_verbosity" = 3 ]; then
        log "$@";
    fi
}

function outv2 () { 
    if [ "$ENV_verbosity" -ge 4 ]; then 
        log "$@"; 
    fi 
} 
 
function outv1 () { 
    if [ "$ENV_verbosity" -ge 3 ]; then 
        log "$@"; 
    fi 
} 

function warn () {
    if [ "$ENV_verbosity" -ge 1 ]; then
        echo "$ENV_me: $@" | tee -a $ENV_log_file 1>&2;
    fi
}

function error () {
    if [ "$ENV_verbosity" -ge 1 ]; then
        warn "$@";
        quit 1;
    fi
}


function outn () {
    if [ "$ENV_verbosity" -ge 2 ]; then
        echo -n "$@";
    fi
}

function out () {
    if [ "$ENV_verbosity" -ge 2 ]; then
        echo "$@";
    fi
}

function outln () {
    if [ "$ENV_verbosity" -ge 2 ]; then
        echo -n "$@" | tee -a $ENV_log_file;
    fi
}

function outl () {
    if [ "$ENV_verbosity" -ge 2 ]; then
        echo "$@" | tee -a $ENV_log_file;
    fi
}

function quit () {
    [ -d "$ENV_tmpdir" ] && rm -rf "$ENV_tmpdir";
    exit $1;
}

function print_version () {
    if [ "$ENV_package" != "" ]; then
        version_str="$ENV_me ($ENV_package) $ENV_version";
    else
        version_str="$ENV_me $ENV_version";
    fi

    cat <<-_VERSION_
		$version_str
		Copyright (C) ${ENV_copyright_years:-$(date +%Y)} ${ENV_author}.
_VERSION_
}

function print_usage () {
    warn "Try \`$ENV_me --help' for more information.";
    if [ "$1" != "0" -a "$1" != "" ]; then
        exit "$1";
    fi
}

function parseargs () {
    unset ${!OPT_@};
    ARGV=($(getopt --shell bash --name $ENV_me \
    --option "$ENV_short_options" \
    --longoptions "$ENV_long_options" \
    -- "$@")) || print_usage 1;

    local index=0;
    while [ "${ARGV[$index]}" != "--" ]; do
        local opt="$(echo ${ARGV[$index]//-/_} | sed 's/^__\?//')";
        eval local arg=${ARGV[$index+1]};   ## eval to get rid of 's

        if [[ "${ARGV[$index+1]}" =~ ^$'\''.* ]]; then # To
##             debug "Setting OPT_$opt = $arg";
            eval OPT_${opt}=$arg;
            index=$((index+1));
        else
##             debug "Incrementing OPT_$opt";
            eval OPT_$opt=$((OPT_$opt+1));
        fi
        index=$((index+1));
    done
    eval ARGV=("${ARGV[@]:$index+1}");
}

function make_tmp_dir () {
    local dirname="$ENV_me.$RANDOM.$RANDOM.$RANDOM.$$";
    ENV_tmpdir="$((umask 077 && \
                   mktemp -d -p ${DEF_tmpdir:-${TMPDIR:-/tmp}} $dirname.XXXXXX) 2>/dev/null)";
    [ -z "$ENV_tmpdir" ] || [ ! -d "$ENV_tmpdir" ] && {
        warn "Unable to create temporary directory. Exiting ..";
        return 1;
    }
    return 0;
}



function init () {
    ENV_verbosity="$DEF_verbosity";
    parseargs "$@";

    [ "$OPT_help" = "1" ] && print_help && quit 0;
    [ "${OPT_version:-$OPT_V}" = "1" ] && print_version && quit 0;

    ENV_verbosity=$(($DEF_verbosity + ${OPT_verbose:-0} + ${OPT_v:-0}));
    [ "${OPT_silent}" = "1" ] && ENV_verbosity=1;
    [ "${OPT_quiet}" = "1" ] && ENV_verbosity=0;

    ENV_log_file=${OPT_log_file:-${OPT_l:-${DEF_log_file}}};
    ENV_backup_dir=${OPT_backup_dir:-${OPT_b:-${DEF_backup_dir}}};
    ENV_password=${OPT_password:-$OPT_p};
    ENV_password_file=${OPT_password_file:-$OPT_P};
    ENV_timestamp=${OPT_timestamp:-$OPT_t};

    if [ ${#ARGV[@]} = 0 ]; then
        warn "missing arguments";
        print_usage 1;
    fi

    if [ ! -z "$ENV_password" ]; then
        PASSWORDS+=("$ENV_password");
    fi

    if [ ! -z "$ENV_password_file" ]; then
        if [ -f "$ENV_password_file" -a -r "$ENV_password_file" ]; then
            saveIFS="$IFS";
            IFS=$'\n';
            PASSWORDS+=( $(cat "$ENV_password_file") );
        else
            warn "$ENV_password_file: Couldn't open password file for reading";
        fi
    fi

    if [ ${#PASSWORDS[@]} = 0 ]; then
        error "No Passwords specified (Use atleast one of -p or -P options)"
    fi

    trap 'quit 255' 1 2 3 6 13 15;
    make_tmp_dir || quit 1;    

    return 0;
}

function decrypt () {
    :
}

function is_encrypted_pdf () {
    arg="$1"
    file "$arg" >/dev/null 2>&1 || {
        warn "$arg: No such file or directory. Skipping ..";
        return 1
    }
    file "$arg" 2>/dev/null | grep -qi pdf || {
        warn "$arg: Not a pdf. Skipping ..";
        return 1
    }
    qpdf --show-encryption "$arg" 2>&1 | grep -q "invalid password" || {
        warn "$arg: Not encrypted. Skipping ..";
        return 1
    }
    return 0
}

function main () {
    for arg in "$@"; do
        basedir=$(dirname "$arg");
        backupdir="$basedir"/"$ENV_backup_dir";
        filename=$(basename "$arg");
        local backupfile="$backupdir/$filename"

        is_encrypted_pdf "$arg" || continue

        echo "$basedir" | grep -q '/' || {
            basedir=".";
        }
        test ! -d "$backupdir" && {
            mkdir -p "$backupdir" || {
                error "\`$ENV_backup_dir': Error while creating backup-dir. Exiting.";
            }
        }

        test -f "$backupfile" && test -z "$ENV_timestamp" && {
            warn "$arg: File exists in backup. Skipping ..";
            continue;
        }

        test "$ENV_timestamp" = 1 && backupfile="$backupfile.$TIMESTAMP"

        cp -i "$arg" "$backupfile" || {
            warn "$arg: Error backing up file. Skipping ..";
            continue;
        }

        oldmd5=$(md5sum "$backupfile" | awk '{print $1}');

        decrypted=0
        for password in "${PASSWORDS[@]}"; do
            outv2 "$arg: Running command:\nqpdf --password=\"$password\" --decrypt \"$backupfile\" \"$arg\"";
            qpdf --password="$password" --decrypt "$backupfile" "$arg" 2> /dev/null;
            newmd5=$(md5sum "$arg" | awk '{print $1}');
            if [ "$newmd5" != "$oldmd5" ]; then
                outv1 "$filename: Found password: $password";
                echo "$password" > "$backupfile".password;
                decrypted=1;
                break;
            fi
        done
        if [ $decrypted -eq 0 ]; then
            warn "$arg: Unable to decrypt file. Skipping ..";
            rm "$backupfile";
        fi
    done
}

init "$@" && main "${ARGV[@]}";
quit $ENV_retval;

